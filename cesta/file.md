---
header: neco1
possiblyUnpopulated: Conditional string populated
possiblyUnpopulatedDisplay: block
---
**file.md**
*Uses default template*

Go to [index](../index.md)

Supports line feed:
one
two
three

<i>html tags enabled (no escaping but cursive)</i>

**table**

abc|def
---|---
1|2
3|4

**link detection**
http://www.seznam.cz

**fenced block**
```
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
```

this is _emphasized_

bullets

* bullet one
* bullet two
* bullet three

~~striked text~~

title left ![obr1](mission.png) title right
