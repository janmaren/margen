<?php
include_once 'Parsedown.inc';

/**
 * Convert markdown file to html, put it to template including other parameters and write to standard out
 *
 * @param string $path path to markdown file (absolute local or http://)
 * @param array configuration of procession. Possible keys: 'defaultTemplateFile'=path to template (local, or http://); 'dateTimeFormat'=date time format
 * @param array parameters for Parsedown library. Possible keys and values: array("breaksEnabled"=>true/false, "markupEscaped"=>true/false, "urlsLinked"=>true/false)
 */
function printHtml($path, $conversionConfig, $markdownConfig) {
    $defaultTemplateFile = $conversionConfig ["defaultTemplateFile"];

    $pageArray = convertMdToHtml ( $path, $markdownConfig, isset ( $conversionConfig ["dateTimeFormat"] ) ? $conversionConfig ["dateTimeFormat"] : "d.m.Y" );
    if ($pageArray === FALSE) {
        header ( "HTTP/1.0 404 Not Found" );
        die ();
    }

    if (! empty ( $pageArray ["template"] )) {
        if (startsWith ( $pageArray ["template"], "http://" ) || startsWith ( $pageArray ["template"], "https://" )) {
            $templateFile = $pageArray ["template"];
        } else {
            $pos = strrpos ( $path, "/" );
            $pathDir = substr ( $path, 0, $pos );
            $templateFile = $pathDir . "/" . $pageArray ["template"];
        }
        unset ( $pageArray ["template"] );
    } else {
        if ($defaultTemplateFile) {
            if (startsWith ( $defaultTemplateFile, "http://" ) || startsWith ( $defaultTemplateFile, "https://" )) {
                $templateFile = $defaultTemplateFile;
            } else {
                $templateFile = getcwd () . "/" . $defaultTemplateFile;
            }
        } else {
            $templateFile = FALSE;
        }
    }

    if ($templateFile !== FALSE) {
        $template = @file_get_contents ( $templateFile );
        // todo: pokud se tohle pro http(s):// soubory nepovede, tak pouzit getHttpContent
    }

    if (! $template) {
        $template = "<html><body>\${content}</body></html>";
    } else {
        if (isAbsolute ( $templateFile )) {
            $pageArray ["templateDirPath"] = stripLastPart ( $path );
        } else {
            $pageArray ["templateDirPath"] = stripLastPart ( getRelativePath ( $path, $templateFile ) );
        }
    }

    $html = getPopulatedText ( $template, $pageArray );
    echo $html;
}

/**
 *
 * @param string $file URL path to file (local, or http://)
 * @param array parameters for Parsedown library. Possible keys and values: array("breaksEnabled"=>true/false, "markupEscaped"=>true/false, "urlsLinked"=>true/false)
 * @param string dateTimeFormat
 * @return array with keys and values from header of Markdown file. Special reserved key is 'content' which contains html content body. All values are to be inserted to template
 */
function convertMdToHtml($file, $markdownConfig, $dateTimeFormat = "d.m.Y") {
    $outputArray = array ();

    $mdFileContent = @file_get_contents ( $file );
    if ($mdFileContent === FALSE) {
        return FALSE;
    }

    $time = @filemtime ( $file );
    if ($time !== FALSE) {
        $outputArray ["date"] = date ( $dateTimeFormat, $time );
    }

    if (preg_match ( "/^[\s]*---\s*(\r\n|\n|\r)/s", $mdFileContent )) {
        $beginHead = strpos ( $mdFileContent, "---" );
        $endHead = strpos ( $mdFileContent, "---", $beginHead + 3 );
        $between = substr ( $mdFileContent, $beginHead + 3, $endHead - $beginHead - 3 );
        $headerStr = trim ( $between );
        populateHeaderArray ( $outputArray, $headerStr );

        $mdFileContent = ltrim ( substr ( $mdFileContent, $endHead + 3 ) );
    }

    $parsedown = new Parsedown ();
    if (isset ( $markdownConfig ["breaksEnabled"] ) && $markdownConfig ["breaksEnabled"]) {
        $parsedown->setBreaksEnabled ( $markdownConfig ["breaksEnabled"] );
    }
    if (isset ( $markdownConfig ["markupEscaped"] ) && $markdownConfig ["markupEscaped"]) {
        $parsedown->setMarkupEscaped ( $markdownConfig ["markupEscaped"] );
    }
    if (isset ( $markdownConfig ["urlsLinked"] ) && $markdownConfig ["urlsLinked"]) {
        $parsedown->setUrlsLinked ( $markdownConfig ["urlsLinked"] );
    }
    $html = $parsedown->parse ( $mdFileContent );
    $outputArray ["content"] = $html;
    return $outputArray;
}

/**
 * Parse header body and add its keys, values to array
 *
 * @param array $outputArray
 * @param string $str
 */
function populateHeaderArray(&$outputArray, $str) {
    $lines = preg_split ( '/\R/', $str );
    foreach ( $lines as $line ) {
        $line = trim ( $line );
        $sepPos = strpos ( $line, ":" );
        if ($sepPos !== false) {
            $key = trim ( substr ( $line, 0, $sepPos ) );
            $value = trim ( substr ( $line, $sepPos + 1 ) );
            $outputArray [$key] = $value;
        }
    }
}
function preg_replacement_quote($str) {
    return preg_replace ( '/(\$|\\\\)(?=\d)/', '\\\\\1', $str );
}

/**
 * Replace placeholders ${NAME[:DEFAULT]} by values
 * Also &lt;!-- :begin{$NAME} and :end{$NAME} --&gt; processed by uncommenting such elements
 *
 * @param string $template
 * @param array $values
 * @return string populated template
 */
function getPopulatedText($template, $values) {
    foreach ( $values as $key => $value ) {
        $template = preg_replace ( '/\$\{' . preg_quote ( $key ) . '((:\w*\})|(\}))/', preg_replacement_quote ( $value ), $template );
        // $template = str_replace('${'.$key.'}', $value, $template);
    }

    $template = preg_replace ( '/\$\{\w*:?([^\}]*)\}/', "$1", $template );
    // $template = preg_replace("/\\\${\\w+}/", "", $template);

    if (strpos ( $template, ":begin{" )) {
        foreach ( $values as $key => $value ) {
            if (trim ( $value ) != '') {
                $template = preg_replace ( '/<!--\\s*:begin{' . preg_quote ( $key ) . '}/', "", $template );
                $template = preg_replace ( '/:end{' . preg_quote ( $key ) . '}\\s*-->/', "", $template );
            }
        }
    }
    return $template;
}
function isAbsolute($path) {
    return startsWith ( $path, "http://" ) || startsWith ( $path, "https://" );
}
function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos ( $haystack, $needle, - strlen ( $haystack ) ) !== FALSE;
}

/**
 * Get relative path to file or directory
 *
 * @param string from from which directory or file (used its directory)
 * @param string to which directory or file
 * @return /margen to /margen/templates: ./templates/
 *         /margen/ to /margen/templates: ./templates/
 *         /margen to /margen/templates/: ./templates/
 *         /margen to /margen/templates/template.md: ./templates/template.md
 *         /margen/templates to /margen: ../
 *         /margen/templates/ to /margen: ../
 *         /margen/templates/ to /margen/: ../
 *         /margen/templates/template.md to /margen/: ../
 *         . to ./templates/template.md: ./templates/template.md
 *         ./templates/template.md to .: ../
 */
function getRelativePath($from, $to) {
    // some compatibility fixes for Windows paths
    $from = is_dir ( $from ) ? rtrim ( $from, '\/' ) . '/' : $from;
    $to = is_dir ( $to ) ? rtrim ( $to, '\/' ) . '/' : $to;
    $from = str_replace ( '\\', '/', $from );
    $to = str_replace ( '\\', '/', $to );

    $from = explode ( '/', $from );
    $to = explode ( '/', $to );
    $relPath = $to;

    foreach ( $from as $depth => $dir ) {
        // find first non-matching dir
        if ($dir === $to [$depth]) {
            // ignore this directory
            array_shift ( $relPath );
        } else {
            // get number of remaining dirs to $from
            $remaining = count ( $from ) - $depth;
            if ($remaining > 1) {
                // add traversals up to first matching dir
                $padLength = (count ( $relPath ) + $remaining - 1) * - 1;
                $relPath = array_pad ( $relPath, $padLength, '..' );
                break;
            } else {
                $relPath [0] = './' . $relPath [0];
            }
        }
    }
    return implode ( '/', $relPath );
}

/**
 * Return last part of path from beginning to last slash (excluding)
 * Or return FALSE when no last part exists
 */
function stripLastPart($path) {
    $pos = strrpos ( $path, "/" );
    if ($pos !== FALSE) {
        return substr ( $path, 0, $pos );
    }
    return FALSE;
}

/**
 * Prototype to be used instead of file_get_contents for remote fetching
 */
function getHttpContent($url, $file) {
    $handle = curl_init ();
    if ($file) {
        $file = fopen ( $file, 'r' );
    }

    curl_setopt ( $handle, CURLOPT_URL, $url );
    curl_setopt ( $handle, CURLOPT_CUSTOMREQUEST, 'GET' );
    curl_setopt ( $handle, CURLOPT_HEADER, true );
    curl_setopt ( $handle, CURLOPT_AUTOREFERER, true );
    curl_setopt ( $handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:13.0) Gecko/20100101 Firefox/13.0.1' );
    if ($file) {
        curl_setopt ( $handle, CURLOPT_FILE, $file );
    }

    curl_exec ( $handle );

    print_r ( curl_getinfo ( $handle ) );

    curl_close ( $handle );
    if ($file) {
        fclose ( $file );
    }
}
?>
